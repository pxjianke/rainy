package rainy.file.synchronization.client.file.traverse;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dongwenbin
 *
 */
public class FileStructure {
	/**
	 * 比较文件异同使用的字段
	 */
	private String hashCode;
	/**
	 * 文件的名字
	 */
	private String fileName;
	/**
	 * 相对配置文件的路径
	 */
	private String filePath;
	private boolean isFile;

	private List<FileStructure> children = new ArrayList<>();

	public String getHashCode() {
		return hashCode;
	}

	public void setHashCode(String hashCode) {
		this.hashCode = hashCode;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public boolean isFile() {
		return isFile;
	}

	public void setFile(boolean isFile) {
		this.isFile = isFile;
	}

	public List<FileStructure> getChildren() {
		return children;
	}

	public void setChildren(List<FileStructure> children) {
		this.children = children;
	}

	public String toString() {
		return "FileStructure [hashCode=" + hashCode + ", fileName=" + fileName + ", filePath=" + filePath + ", isFile="
				+ isFile + ", children=" + children + "]";
	}
}
