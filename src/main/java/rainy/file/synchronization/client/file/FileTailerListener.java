package rainy.file.synchronization.client.file;

import rainy.file.synchronization.client.FileTailerSendClient;
import rainy.file.synchronization.config.RainyRunTime;

public class FileTailerListener {

	public void newLogFileLine(String fileName, String line) {
		if (line == null || line.trim().length() == 0) {
			return;
		}
		line = fileName + RainyRunTime.COMMAND_SPLIT + line;
		FileTailerSendClient.addNewAddedContent(line);
		System.out.println("add new line : " + line);
	}

}
