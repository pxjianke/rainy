#rainy 
外面正在下雨，下雨天比较适合编程奥，所以创造了Rainy.

Rainy的含义就是下雨，下雨就是从各种地方来的水汇入到大地的过程，所以Rainy做的事情也一样，就是把四处的文件汇总到某个大地服务器，比如你的静态资源。

[我的博客](http://my.oschina.net/dwbin/blog/666860)
[QQ group : 539994816](#)

#<img src="http://git.oschina.net/grom/rainy/raw/master/bin/%E6%96%87%E4%BB%B6%E9%83%A8%E7%BD%B2%E5%9B%BE.bmp"/>

#应用场景
中小型的静态资源同步;:
比如，你部署了多台后端服务器，但是有个上传资源的地方，这种时候怎么让前端可以展示刚刚上传的资源呢？当然可以使用CMS，但是当我们还没有成长为一个大软件的时候，Rainy是一个不错的选择，通过把Rainy分别部署为Master跟Slave，可以方便的将Slave上面的资源同步到Master节点，这时候只要所有的静态资源访问都被导向到Master节点，那么自然上传的文件就是可以访问到的。

#使用需求
1. 要求JDK 1.7以上，因为使用了JDK 1.7的API，所以请使用1.7以上运行环境
2. 使用maven管理依赖
3. 依赖于Netty

#项目运行方式

- 从代码运行
    该项目为maven项目，下载代码之后直接导入到eclipse即可。启动的方式为下面的两种方式：

启动Master

` 
rainy.file.synchronization.server.MonitorServer
`

启动Master

`
rainy.file.synchronization.client.FileSendClient
`

- 直接从二进制运行
    项目下面有个bin目录，下载后直接进入bin目录有bat文件可以让你运行服务器和client
    
#项目集成方式
对于普通的java项目或者其他类型的项目，可以采用从二进制代码运行的方式来启动服务器即可。
Linux 可以拷贝bin目录，并使用下面的命令：

##启动服务器
` 
nohup java -cp .:netty-all-5.0.0.Alpha1.jar:commons-codec-1.9.jar:synchronization-0.0.2-SNAPSHOT.jar rainy.file.synchronization.server.MonitorServer &
`

##启动Slave文件同步
`
nohup java -cp .:netty-all-5.0.0.Alpha1.jar:commons-codec-1.9.jar:synchronization-0.0.2-SNAPSHOT.jar rainy.file.synchronization.client.FileSendClient &
`
##启动Slave 文件tail
`
nohup java -cp .:netty-all-5.0.0.Alpha1.jar:commons-codec-1.9.jar:synchronization-0.0.2-SNAPSHOT.jar rainy.file.synchronization.client.FileTailerSendClient &
`

    对于任意配置文件的修改，请修改bin下面的rainy.properties即可

##对于web项目
如果你需要自己决定哪台服务器作为master请自己编写代码并调用上面的FileSendClient和MonitorServer即可。
如果你不想自己决定，那么可以通过APP类进行启动。
master的决定方式参照如下顺序，
首先根据你定义的rainy.properties中的rainy.master.serverName来检查本机的servername是否一致，如果一致则为master。
如果不一致，那么将会获取本机所有的网卡信息，并对比IP，如果IP中有一个跟你的serverName一致，那么则为master，
如果以上都不满足则为slave



#联系方式
This is Grom, can be contacted with wenbindong1984@163.com or join the QQ group : 539994816

#版本信息:

0.0.1 -- 完成文件同步


0.0.2 -- 增加tail文件汇总